# OCLIDE 

![Program screenshot](https://raw.githubusercontent.com/Vladg24YT/Oclide/gh-pages/images/screenshots/OCLIDE_screenshot.png) 
Oclide is an INDEV IDE for OpenComputers 1.7.5 written in Java 8

## Feature list
- [x] Editing tools (Undo/Redo, Clipboard)
- [x] Syntax highlighting
- [x] Code autocompletion *(ongoing)*
- [x] Internationalization *(ongoing)*

## Requirements
- JRE/JDK 1.8 or later

## Installation
1. Clone the repo: `git clone https://gitlab.com/Vladg24YT/oclide.git` 
2. Copy `.jar` to any folder you want. It will be OCLIDE's working directory.
3. Run `java -jar OCLIDE.jar`

## Contributing
Feel free to create push requests with translation files.  
The project is also open to issues regarding bugs, incorrectly working things or feature requests.
