/*
 * The MIT License
 *
 * Copyright 2021 Vladislav Gorskii.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package ru.VladTheMountain.oclide.api.parser;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Queue;
import org.apache.commons.collections4.queue.CircularFifoQueue;

/**
 * <code>The lexer reads characters from the input/character reader and produces
 * tokens. In other words, it converts a character stream into a token stream.
 * Hence its sometimes also called the tokenizer. These tokens are produced
 * according to the defined grammar.</code>
 *
 * @author VladTheMountain
 */
public class Lexer {

    private InputReader input;

    private Queue<LexToken> ringBuffer;

    //States
    private static final int NULL = 1;
    private static final int TOKEN = 2;
    private static final int SPECIAL_TOKEN = 3;
    private static final int EOF = 4;
    //End of States

    /**
     * Initializes the {@code InputReader}, which is necessary for peeking and
     * consuming methods
     *
     * @param filePath Path to file to get code from
     * @throws FileNotFoundException if the specified file could not be found
     */
    public Lexer(String filePath) throws FileNotFoundException {
        this(new File(filePath));
    }

    /**
     * Initializes the {@code InputReader}, which is necessary for peeking and
     * consuming methods
     *
     * @param file The file to get code from
     * @throws FileNotFoundException if the specified file could not be found
     */
    public Lexer(File file) throws FileNotFoundException {
        input = new InputReader(file);
        int bufferSize = 256;
        ringBuffer = new CircularFifoQueue<>(bufferSize);
        String lexerState = LexToken.TYPE_CHUNK;
    }

    /**
     * Get the next token from the input.This is used to look ahead the tokens
     * without consuming/removing them from the input stream. Calling the
     * {@code peek()} method more than once will return the same token.
     *
     * @return The next character from the input
     * @throws java.io.IOException if an I/O error occurs
     * @see #peek(int)
     */
    public LexToken peek() throws IOException {
        ringBuffer.add(nextToken());
        return ringBuffer.peek();
    }

    /**
     * Get the next k-th token from the input. This is used to look ahead the
     * tokens without consuming/removing them from the input stream. Calling the
     * {@code peek()} method more than once will return the same token.
     *
     * @param k the index of the character to get
     * @return The k-th character from the input
     * @see #peek()
     */
    public LexToken peek(int k) {
        return (LexToken) ringBuffer.toArray()[k];
    }

    /**
     * Get the next token from the input, and remove it from the input. This
     * means, calling the {@code consume()} method multiple times will return a
     * new character at each invocation.
     *
     * @return The next character from the input
     * @see #consume(int)
     */
    public LexToken consume() {
        throw new UnsupportedOperationException("Not implemented yet");
    }

    /**
     * Get the next k-th token from the input, and remove it from the input.
     * This means, calling the {@code consume()} method multiple times will
     * return a new character at each invocation.
     *
     * @param k the index of the character to get
     * @return The k-th character from the input
     * @see #consume()
     */
    public LexToken consume(int k) {
        throw new UnsupportedOperationException("Not implemented yet");
    }

    /**
     * Private method, containing all the logic of the lexer. It changes the
     * lexer's state and launches the necessary processing functions depending
     * on the grammar rules of the language.
     *
     * @throws java.io.IOException if an I/O error occurs
     */
    private LexToken nextToken() throws IOException {
        if (input.peek() == 'n'
                && input.peek(input.currentPos + 1) == 'i'
                && input.peek(input.currentPos + 1) == 'l') {
            return processNil();
        } else if (input.peek() == 'f'
                && input.peek(input.currentPos + 1) == 'a'
                && input.peek(input.currentPos + 2) == 'l'
                && input.peek(input.currentPos + 3) == 's'
                && input.peek(input.currentPos + 4) == 'e') {
            return processFalse();
        } else if (input.peek() == 'f'
                && input.peek(input.currentPos + 1) == 'u'
                && input.peek(input.currentPos + 2) == 'n'
                && input.peek(input.currentPos + 3) == 'c'
                && input.peek(input.currentPos + 4) == 't'
                && input.peek(input.currentPos + 1) == 'i'
                && input.peek(input.currentPos + 2) == 'o'
                && input.peek(input.currentPos + 3) == 'n') {
            return processFunction();
        } else if (input.peek() == 't'
                && input.peek(input.currentPos + 1) == 'r'
                && input.peek(input.currentPos + 2) == 'u'
                && input.peek(input.currentPos + 3) == 'e') {
            return processTrue();
        } else if (input.peek() == '0'
                || input.peek() == '1'
                || input.peek() == '2'
                || input.peek() == '3'
                || input.peek() == '4'
                || input.peek() == '5'
                || input.peek() == '6'
                || input.peek() == '7'
                || input.peek() == '8'
                || input.peek() == '9') {
            return processNumeral();
        } else if (input.peek() == '"') {
            return processLiteralString();
        } else if (input.peek() == '.' && input.peek(input.currentPos + 1) == '.'
                && input.peek(input.currentPos + 2) == '.') {
            return processTripleDot();
        } else {
            return processChunk();
        }
        //TODO: Finish the algorithm
    }
    // WARNING: LESS-DOCUMENTED PROCESSING FUNCTIONS AHEAD

    private LexToken processNil() {
        return null;
    }

    private LexToken processFalse() {
        return null;
    }

    private LexToken processTrue() {
        return null;
    }

    private LexToken processNumeral() {
        return null;
    }

    private LexToken processLiteralString() {
        return null;
    }

    private LexToken processTripleDot() {
        return null;
    }

    private LexToken processFunction() {
        return null;
    }

    private LexToken processPrefix() {
        return null;
    }

    private LexToken processTable() {
        return null;
    }

    private LexToken processBinOP() {
        return null;
    }

    private LexToken processUnOP() {
        return null;
    }

    private LexToken processChunk() {
        return null;
    }
}
