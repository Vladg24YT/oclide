/*
 * The MIT License
 *
 * Copyright 2021 Vladislav Gorskii.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
/**
 * An attempt to create a Lua 5.3 parser in Java using
 * <a href="https://medium.com/swlh/writing-a-parser-getting-started-44ba70bb6cc9">Supun
 * Setunga's "Writing a Parser"</a> articles.
 * 
 * <br><br><i>Might make it a standalone API later, but idk as for now</i>
 *
 * @author VladTheMountain
 * 
 * @see ru.VladTheMountain.oclide.api.parser.InputReader
 * @see ru.VladTheMountain.oclide.api.parser.Lexer
 * @see ru.VladTheMountain.oclide.api.parser.Parser
 * @see ru.VladTheMountain.oclide.api.parser.ParserErrorHandler
 */
package ru.VladTheMountain.oclide.api.parser;
