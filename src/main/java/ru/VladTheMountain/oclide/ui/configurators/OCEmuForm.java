/*
 * The MIT License
 *
 * Copyright 2021 Vladislav Gorskii.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package ru.VladTheMountain.oclide.ui.configurators;

import org.apache.commons.io.FileUtils;
import ru.VladTheMountain.oclide.api.ocemu.OCEmuConfig;
import ru.VladTheMountain.oclide.api.launcher.OCEmuLauncher;
import ru.VladTheMountain.oclide.api.ocemu.component.OCEmuComponent;
import ru.VladTheMountain.oclide.api.util.UUIDGenerator;

import javax.swing.*;
import javax.swing.border.TitledBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.filechooser.FileFilter;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.Locale;
import java.util.Objects;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * OCEmu by James Coonradt and Zen1th
 *
 * Emulator configurator UI
 *
 * @author VladTheMountain
 */
public class OCEmuForm extends javax.swing.JFrame {

    final ResourceBundle localizationResource = ResourceBundle.getBundle("configurator.ocemu.OCEmu", Locale.getDefault());

    private OCEmuComponent[] componentsArray = {};

    /**
     * Creates new form ConfiguratorForm
     */
    public OCEmuForm() {
        Timer t = new Timer(300, (ActionEvent e) -> this.repaint());
        initComponents();
        if (!(new File("OCEmu/.machine/ocemu.cfg").exists()) || this.componentsArray == null || this.componentsArray.length == 0) {
            this.componentsArray = new OCEmuComponent[OCEmuLauncher.DEFAULT.length];
            System.arraycopy(OCEmuLauncher.DEFAULT, 0, componentsArray, 0, OCEmuLauncher.DEFAULT.length);
        } else {
            new OCEmuConfig(this.componentsArray).readConfig(new File("OCEmu/.machine/ocemu.cfg"));
        }
        updateComponentList();
        t.start();
    }

    /**
     * Adds a new component to the list
     *
     * @param c an OCEmuComponent to add
     * @see ru.VladTheMountain.oclide.api.ocemu.component.OCEmuComponent
     */
    private void addComponent(OCEmuComponent c) {
        OCEmuComponent[] tmp = new OCEmuComponent[this.componentsArray.length + 1];
        System.arraycopy(this.componentsArray, 0, tmp, 0, this.componentsArray.length);
        tmp[this.componentsArray.length] = c;
        this.componentsArray = new OCEmuComponent[tmp.length];
        System.arraycopy(tmp, 0, this.componentsArray, 0, tmp.length);
        updateComponentList();
    }

    /**
     * Deletes a component at {@code index}
     *
     * @param index the index of the component to delete
     */
    private void deleteComponent(int index) {
        OCEmuComponent[] tmp = new OCEmuComponent[this.componentsArray.length];
        System.arraycopy(this.componentsArray, 0, tmp, 0, tmp.length);
        this.componentsArray = new OCEmuComponent[tmp.length - 1];
        for (int i = 0; i < this.componentsArray.length; i++) {
            this.componentsArray[i] = i < index ? tmp[i] : tmp[i + 1];
        }
    }

    /**
     * Updates componentList {@code JTree}
     */
    private void updateComponentList() {
        DefaultListModel<String> model = new DefaultListModel<>();
        for (int i = 0; i < componentsArray.length; i++) {
            model.insertElementAt(this.componentTypeComboBox.getItemAt(this.componentsArray[i].getComponentType() + 1), i);
        }
        this.componentList.setModel(model);
    }

    /**
     * Copies OpenOS from OCEmu's loot folder to target filesystem
     */
    private void installOpenOS() {
        String input = "tmpfs";
        File machineDir = new File("OCEmu/.machine/" + input);
        try {
            FileUtils.copyDirectory(new File("OCEmu/loot/openos/bin"), new File(machineDir.getAbsoluteFile() + "/bin"));
            FileUtils.copyDirectory(new File("OCEmu/loot/openos/boot"), new File(machineDir.getAbsoluteFile() + "/boot"));
            FileUtils.copyDirectory(new File("OCEmu/loot/openos/etc"), new File(machineDir.getAbsoluteFile() + "/etc"));
            FileUtils.copyDirectory(new File("OCEmu/loot/openos/home"), new File(machineDir.getAbsoluteFile() + "/home"));
            FileUtils.copyDirectory(new File("OCEmu/loot/openos/lib"), new File(machineDir.getAbsoluteFile() + "/lib"));
            FileUtils.copyDirectory(new File("OCEmu/loot/openos/usr"), new File(machineDir.getAbsoluteFile() + "/usr"));
            Files.copy(new File("OCEmu/loot/openos/.prop").toPath(), new File(machineDir.getAbsoluteFile() + "/.prop").toPath(), StandardCopyOption.REPLACE_EXISTING);
            Files.copy(new File("OCEmu/loot/openos/init.lua").toPath(), new File(machineDir.getAbsoluteFile() + "/init.lua").toPath(), StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException ex) {
            Logger.getLogger(OCEmuForm.class.getName()).log(Level.SEVERE, "Error when installing OpenOS", ex);
            JOptionPane.showMessageDialog(null, ex, "Caught " + ex.getClass().getName(), JOptionPane.ERROR_MESSAGE);
        }
        Logger.getLogger(this.getClass().getName()).log(Level.FINER, "OpenOS is installed");
    }

    /**
     * Updates description and option fields
     *
     * @param c the OCEmuComponent to view
     */
    private void updateFields(OCEmuComponent c) {
        switch (c.getComponentType()) {
            case 0:
                //Description set
                this.componentDescriptionArea.setText("Computer represents the \n'computer' library.");
                //Option change
                this.option1Field.setText("");
                this.option2Field.setText("");
                this.option3Field.setText("");
                this.option4Field.setText("");
                //Option management
                this.option1Field.setEditable(false);
                this.option2Field.setEditable(false);
                this.option3Field.setEditable(false);
                this.option4Field.setEditable(false);
                break;
            case 1:
                this.componentDescriptionArea.setText("EEPROM is responsible for \nbooting up the machine.");
                //Option change
                this.option1Field.setText(c.getOptionAt(0));
                this.option2Field.setText(c.getOptionAt(1));
                this.option3Field.setText("");
                this.option4Field.setText("");
                //Option management
                this.option1Field.setEditable(true);
                this.option2Field.setEditable(true);
                this.option3Field.setEditable(false);
                this.option4Field.setEditable(false);
                break;
            case 2:
                this.componentDescriptionArea.setText("Filesystem is responsible \nfor file management \nand represents the 'filesystem' \nlibrary.");
                //Option change
                this.option1Field.setText(c.getOptionAt(0));
                this.option2Field.setText(c.getOptionAt(1));
                this.option3Field.setText(c.getOptionAt(2));
                this.option4Field.setText("");
                //Option management
                this.option1Field.setEditable(true);
                this.option2Field.setEditable(true);
                this.option3Field.setEditable(true);
                this.option4Field.setEditable(false);
                break;
            case 3:
                this.componentDescriptionArea.setText("GPU is responsible for screen \nbuffer manipulations \nand represents the 'gpu' \ncomponent library.");
                //Option change
                this.option1Field.setText(c.getOptionAt(0));
                this.option2Field.setText(c.getOptionAt(1));
                this.option3Field.setText(c.getOptionAt(2));
                this.option4Field.setText(c.getOptionAt(3));
                //Option management
                this.option1Field.setEditable(true);
                this.option2Field.setEditable(true);
                this.option3Field.setEditable(true);
                this.option4Field.setEditable(true);
                break;
            case 4:
                this.componentDescriptionArea.setText("Internet is responsible for \nHTTP-requests and \nrepresents the 'internet' \ncomponent library.");
                //Option change
                this.option1Field.setText("");
                this.option2Field.setText("");
                this.option3Field.setText("");
                this.option4Field.setText("");
                //Option management
                this.option1Field.setEditable(false);
                this.option2Field.setEditable(false);
                this.option3Field.setEditable(false);
                this.option4Field.setEditable(false);
                break;
            case 5:
                this.componentDescriptionArea.setText("Keyboard is responsible for \nuser's input and \nrepresents the 'keyboard' \ncomponent library.");
                //Option change
                this.option1Field.setText("");
                this.option2Field.setText("");
                this.option3Field.setText("");
                this.option4Field.setText("");
                //Option management
                this.option1Field.setEditable(false);
                this.option2Field.setEditable(false);
                this.option3Field.setEditable(false);
                this.option4Field.setEditable(false);
                break;
            case 6:
                this.componentDescriptionArea.setText("Modem is responsible for \ncross-machine networking \nand represents the 'modem' \ncomponent library.");
                //Option change
                this.option1Field.setText(c.getOptionAt(0));
                this.option2Field.setText(c.getOptionAt(1));
                this.option3Field.setText("");
                this.option4Field.setText("");
                //Option management
                this.option1Field.setEditable(true);
                this.option2Field.setEditable(true);
                this.option3Field.setEditable(false);
                this.option4Field.setEditable(false);
                break;
            case 7:
                this.componentDescriptionArea.setText("OCEmu is responsible for \nintegration of OCEmu's code \nwith original OpenComputers'\n code.");
                //Option change
                this.option1Field.setText("");
                this.option2Field.setText("");
                this.option3Field.setText("");
                this.option4Field.setText("");
                //Option management
                this.option1Field.setEditable(false);
                this.option2Field.setEditable(false);
                this.option3Field.setEditable(false);
                this.option4Field.setEditable(false);
                break;
            case 8:
                this.componentDescriptionArea.setText("Screen is responsible for \nthe screen block and \nrepresents the 'screen' library.");
                //Option change
                this.option1Field.setText(c.getOptionAt(0));
                this.option2Field.setText(c.getOptionAt(1));
                this.option3Field.setText(c.getOptionAt(2));
                this.option4Field.setText(c.getOptionAt(3));
                //Option management
                this.option1Field.setEditable(true);
                this.option2Field.setEditable(true);
                this.option3Field.setEditable(true);
                this.option4Field.setEditable(true);
                break;
            default:
                this.componentDescriptionArea.setText("Couldn't read the component");
                //Option change
                this.option1Field.setText("");
                this.option2Field.setText("");
                this.option3Field.setText("");
                this.option4Field.setText("");
                //Option management
                this.option1Field.setEditable(false);
                this.option2Field.setEditable(false);
                this.option3Field.setEditable(false);
                this.option4Field.setEditable(false);
                break;
        }
    }

    /**
     * Recursively gets all files in the project folder
     *
     * @param src Project folder as a {@code File}
     * @param targ Project's tree node
     */
    private static void recursivelyCopyFiles(File src, File targ) {
        File[] files = src.listFiles();
        if (files == null) {
            return;
        }
        for (File f : files) {
            if (f.isFile()) {
                try {
                    Files.copy(f.toPath(), new File(targ.getAbsolutePath() + FileSystems.getDefault().getSeparator() + f.getName()).toPath());
                } catch (IOException ex) {
                    Logger.getLogger(OCEmuForm.class.getName()).log(Level.SEVERE, "Error when copying file " + f.getPath(), ex);
                    JOptionPane.showMessageDialog(null, ex, "Caught " + ex.getClass().getName(), JOptionPane.ERROR_MESSAGE);
                }
            } else {
                try {
                    FileUtils.copyDirectory(f, targ);
                } catch (IOException ex) {
                    Logger.getLogger(OCEmuForm.class.getName()).log(Level.SEVERE, "Error when copying folder " + f.getPath(), ex);
                    JOptionPane.showMessageDialog(null, ex, "Caught " + ex.getClass().getName(), JOptionPane.ERROR_MESSAGE);
                }
            }
            if (f.isDirectory()) {
                recursivelyCopyFiles(src, targ);
            }
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        configChooser = new JFileChooser();
        JPanel controlPanel = new JPanel();
        JButton launchButton = new JButton();
        // Variables declaration - do not modify//GEN-BEGIN:variables
        JButton cancelButton = new JButton();
        JPanel componentSettingsPanel = new JPanel();
        JLabel jLabel1 = new JLabel();
        componentAddressField = new JTextField();
        JLabel jLabel2 = new JLabel();
        option1Field = new JTextField();
        JLabel jLabel3 = new JLabel();
        option2Field = new JTextField();
        JLabel jLabel4 = new JLabel();
        option3Field = new JTextField();
        JPanel descriptionPanel = new JPanel();
        JScrollPane jScrollPane2 = new JScrollPane();
        componentDescriptionArea = new JTextArea();
        JLabel jLabel5 = new JLabel();
        componentTypeComboBox = new JComboBox<>();
        JLabel jLabel6 = new JLabel();
        option4Field = new JTextField();
        JScrollPane jScrollPane1 = new JScrollPane();
        componentList = new JList<>();
        JMenuBar jMenuBar1 = new JMenuBar();
        JMenu fileMenu = new JMenu();
        JMenuItem resetConfigItem = new JMenuItem();
        JPopupMenu.Separator jSeparator1 = new JPopupMenu.Separator();
        JMenuItem importConfigItem = new JMenuItem();
        JMenuItem saveConfigItem = new JMenuItem();
        JPopupMenu.Separator jSeparator2 = new JPopupMenu.Separator();
        JMenuItem exitItem = new JMenuItem();
        JMenu componentMenu = new JMenu();
        JMenuItem jMenuItem1 = new JMenuItem();
        JMenuItem jMenuItem2 = new JMenuItem();
        JMenu helpMenu = new JMenu();

        configChooser.setDialogTitle("Open config...");
        configChooser.setFileFilter(new FileFilter() {
            @Override
            public boolean accept(File f){
                return f.getName().equals("ocemu.cfg");
            }

            @Override
            public String getDescription(){
                return "OCEmu config file ('ocemu.cfg')";
            }
        }
    );
    configChooser.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);

    setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
    setTitle("Configure OCEmu");
    setResizable(false);

    launchButton.setText("Launch");
    launchButton.addActionListener(this::launchButtonActionPerformed);

    cancelButton.setText("Cancel");
    cancelButton.addActionListener(this::cancelButtonActionPerformed);

        GroupLayout controlPanelLayout = new GroupLayout(controlPanel);
    controlPanel.setLayout(controlPanelLayout);
    controlPanelLayout.setHorizontalGroup(controlPanelLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
        .addGroup(GroupLayout.Alignment.TRAILING, controlPanelLayout.createSequentialGroup()
            .addContainerGap()
            .addComponent(cancelButton)
            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(launchButton)
            .addContainerGap())
    );
    controlPanelLayout.setVerticalGroup(controlPanelLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
        .addGroup(GroupLayout.Alignment.TRAILING, controlPanelLayout.createSequentialGroup()
            .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(controlPanelLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                .addComponent(launchButton)
                .addComponent(cancelButton))
            .addContainerGap())
    );

    jLabel1.setText("Component's address:");

    componentAddressField.setEditable(false);

    jLabel2.setText("Option 1");

    jLabel3.setText("Option 2");

    jLabel4.setText("Option 3");

    descriptionPanel.setBorder(BorderFactory.createTitledBorder(null, "Description", TitledBorder.RIGHT, TitledBorder.TOP, new Font("Segoe UI", Font.BOLD, 10))); // NOI18N

    jScrollPane2.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);

    componentDescriptionArea.setEditable(false);
    componentDescriptionArea.setColumns(20);
    componentDescriptionArea.setFont(new Font("Monospaced", Font.PLAIN, 12)); // NOI18N
    componentDescriptionArea.setRows(5);
    componentDescriptionArea.setWrapStyleWord(true);
    jScrollPane2.setViewportView(componentDescriptionArea);

        GroupLayout descriptionPanelLayout = new GroupLayout(descriptionPanel);
    descriptionPanel.setLayout(descriptionPanelLayout);
    descriptionPanelLayout.setHorizontalGroup(descriptionPanelLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
        .addComponent(jScrollPane2)
    );
    descriptionPanelLayout.setVerticalGroup(descriptionPanelLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
        .addGroup(descriptionPanelLayout.createSequentialGroup()
            .addComponent(jScrollPane2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
            .addGap(0, 0, Short.MAX_VALUE))
    );

    jLabel5.setText("Component Type:");

    componentTypeComboBox.setModel(new DefaultComboBoxModel<>(new String[] { "computer", "cpu", "eeprom", "filesystem", "gpu", "internet", "keyboard", "modem", "ocemu", "screen", " " }));
    componentTypeComboBox.addActionListener(this::componentTypeComboBoxActionPerformed);

    jLabel6.setText("Option 4");

        GroupLayout componentSettingsPanelLayout = new GroupLayout(componentSettingsPanel);
    componentSettingsPanel.setLayout(componentSettingsPanelLayout);
    componentSettingsPanelLayout.setHorizontalGroup(componentSettingsPanelLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
        .addGroup(componentSettingsPanelLayout.createSequentialGroup()
            .addContainerGap()
            .addGroup(componentSettingsPanelLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
                .addComponent(descriptionPanel, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(GroupLayout.Alignment.TRAILING, componentSettingsPanelLayout.createSequentialGroup()
                    .addGroup(componentSettingsPanelLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
                        .addComponent(jLabel1)
                        .addComponent(jLabel2)
                        .addComponent(jLabel5)
                        .addComponent(jLabel3)
                        .addComponent(jLabel4)
                        .addComponent(jLabel6))
                    .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                    .addGroup(componentSettingsPanelLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
                        .addComponent(option3Field)
                        .addComponent(option2Field)
                        .addComponent(componentTypeComboBox, 0, 208, Short.MAX_VALUE)
                        .addComponent(option1Field)
                        .addComponent(option4Field)
                        .addComponent(componentAddressField, GroupLayout.Alignment.TRAILING))))
            .addContainerGap())
    );
    componentSettingsPanelLayout.setVerticalGroup(componentSettingsPanelLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
        .addGroup(componentSettingsPanelLayout.createSequentialGroup()
            .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(componentSettingsPanelLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                .addComponent(jLabel5)
                .addComponent(componentTypeComboBox, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(componentSettingsPanelLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                .addComponent(jLabel1)
                .addComponent(componentAddressField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(componentSettingsPanelLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                .addComponent(jLabel2)
                .addComponent(option1Field, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(componentSettingsPanelLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                .addComponent(option2Field, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addComponent(jLabel3))
            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(componentSettingsPanelLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                .addComponent(jLabel4)
                .addComponent(option3Field, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(componentSettingsPanelLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                .addComponent(jLabel6)
                .addComponent(option4Field, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
            .addComponent(descriptionPanel, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
            .addContainerGap())
    );

    componentList.setModel(new AbstractListModel<String>() {
        final String[] strings = { "gpu", "modem", "eeprom", "filesystem", "filesystem", "filesystem", "internet", "computer", "ocemu", "screen", "keyboard" };
        public int getSize() { return strings.length; }
        public String getElementAt(int i) { return strings[i]; }
    });
    componentList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    componentList.addListSelectionListener(this::componentListValueChanged);
    jScrollPane1.setViewportView(componentList);

    fileMenu.setText("File");

    resetConfigItem.setText("Reset config");
    resetConfigItem.addActionListener(this::resetConfigItemActionPerformed);
    fileMenu.add(resetConfigItem);
    fileMenu.add(jSeparator1);

    importConfigItem.setText("Import from file");
    importConfigItem.addActionListener(this::importConfigItemActionPerformed);
    fileMenu.add(importConfigItem);

    saveConfigItem.setText("Save to file");
    saveConfigItem.addActionListener(this::saveConfigItemActionPerformed);
    fileMenu.add(saveConfigItem);
    fileMenu.add(jSeparator2);

    exitItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0));
    exitItem.setText("Exit");
    exitItem.addActionListener(this::exitItemActionPerformed);
    fileMenu.add(exitItem);

    jMenuBar1.add(fileMenu);

    componentMenu.setText("Component");

    jMenuItem1.setText("Add");
    jMenuItem1.addActionListener(this::jMenuItem1ActionPerformed);
    componentMenu.add(jMenuItem1);

    jMenuItem2.setText("Delete selected");
    jMenuItem2.addActionListener(this::jMenuItem2ActionPerformed);
    componentMenu.add(jMenuItem2);

    jMenuBar1.add(componentMenu);

    helpMenu.setText("Help");
    jMenuBar1.add(helpMenu);

    setJMenuBar(jMenuBar1);

        GroupLayout layout = new GroupLayout(getContentPane());
    getContentPane().setLayout(layout);
    layout.setHorizontalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
        .addGroup(layout.createSequentialGroup()
            .addContainerGap()
            .addComponent(jScrollPane1, GroupLayout.PREFERRED_SIZE, 190, GroupLayout.PREFERRED_SIZE)
            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
            .addComponent(componentSettingsPanel, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        .addComponent(controlPanel, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
    );
    layout.setVerticalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
        .addGroup(layout.createSequentialGroup()
            .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                .addComponent(componentSettingsPanel, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(jScrollPane1)))
            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
            .addComponent(controlPanel, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
    );

    pack();
    setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void componentListValueChanged(ListSelectionEvent evt) {//GEN-FIRST:event_componentListValueChanged
        int s = this.componentList.getSelectedIndex();
        if (s >= 0) {
            this.componentTypeComboBox.setSelectedIndex(componentsArray[s].getComponentType() + 1);
            this.componentAddressField.setText(componentsArray[s].getComponentAddress());
            updateFields(componentsArray[s]);
        }
    }//GEN-LAST:event_componentListValueChanged

    private void cancelButtonActionPerformed(ActionEvent evt) {//GEN-FIRST:event_cancelButtonActionPerformed
        this.dispose();
    }//GEN-LAST:event_cancelButtonActionPerformed

    private void launchButtonActionPerformed(ActionEvent evt) {//GEN-FIRST:event_launchButtonActionPerformed
        Logger.getLogger(this.getClass().getName()).log(Level.INFO, "Setting up OCEmu workspace...");
        System.out.println("Setting up OCEmu workspace...");
        boolean isOSInstalled = false;
        String OpenOSUUID = "tmpfs";
        File machineFolder = new File("OCEmu" + FileSystems.getDefault().getSeparator() + ".machine");
        // The following code should search through all the directories near the OCLIDE.jar to find 'init.lua' file
        if (machineFolder.exists() && Objects.requireNonNull(machineFolder.listFiles()).length > 0) {
            for (File file : Objects.requireNonNull(machineFolder.listFiles())) {
                if (file.isDirectory()) {
                    for (String current : Objects.requireNonNull(file.list())) {
                        //Well, technically it means that ANY OS, compatible with Lua BIOS EEPROM bootloader script can be installed
                        if (current.equals("init.lua")) {
                            isOSInstalled = true;
                            OpenOSUUID = file.getName();
                        }
                    }
                }
            }
            // If no OS is installed, installing OpenOS
            if (isOSInstalled) {
                Logger.getLogger(this.getClass().getName()).log(Level.FINE, "Found a Lua BIOS-compatible OS");
            } else {
                Logger.getLogger(this.getClass().getName()).log(Level.FINE, "No OS found. installing OpenOS");
                installOpenOS();
            }
        } else {
            machineFolder.mkdirs();
            Logger.getLogger(this.getClass().getName()).log(Level.FINE, "No filesystem found, creating...");
        }
        // Creating OCEmu config
        new OCEmuConfig(this.componentsArray).createConfig();
        System.out.println("Config ready");
        Logger.getLogger(this.getClass().getName()).log(Level.INFO, "OCEmu config ready");
        // Copying all projects' files
        if (new File("projects").exists()) {
            try {
                //recursivelyCopyFiles(new File("projects"), new File("OCEmu"+FileSystems.getDefault().getSeparator()+".machine"+FileSystems.getDefault().getSeparator()+OpenOSUUID+FileSystems.getDefault().getSeparator()+"home"));
                FileUtils.copyDirectory(new File("projects"), new File(/*System.getenv("APPDATA") + FileSystems.getDefault().getSeparator() + "OCEmu"*/".machine" + FileSystems.getDefault().getSeparator() + OpenOSUUID + FileSystems.getDefault().getSeparator() + "home"));
                Logger.getLogger(this.getClass().getName()).log(Level.FINEST, "Copied " + new File("projects").getAbsolutePath() + " to " + new File(/*System.getenv("APPDATA") + FileSystems.getDefault().getSeparator() + "OCEmu"*/".machine" + FileSystems.getDefault().getSeparator() + OpenOSUUID + FileSystems.getDefault().getSeparator() + "home").getAbsolutePath());
            } catch (IOException ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "Failed to copy projects' files", ex);
                JOptionPane.showMessageDialog(null, ex, "Caught " + ex.getClass().getName(), JOptionPane.ERROR_MESSAGE);
            }
        }
        System.out.println("Files copied");
        Logger.getLogger(this.getClass().getName()).log(Level.FINE, "Project files copied");
        // Creating a new thread, where OCEmu will be launched
        Thread t = new Thread() {
            @Override
            public void run() {
                // Creating a process
                ProcessBuilder pb;
                pb = System.getProperty("os.name").contains("Windows") ? new ProcessBuilder("cmd.exe", "/c", "start", "/D", "OCEmu", "OCEmu\\OCEmu.exe") : new ProcessBuilder("lua", "OCEmu/boot.lua", "./.machine");
                pb.redirectErrorStream(true).inheritIO();
                try {
                    // Starting the process...
                    Process p = pb.start();
                    Logger.getLogger(this.getClass().getName()).log(Level.INFO, "Starting OCEmu...");
                    System.out.println("Starting OCEmu...");
                    // ...and rerouting the output into System.Out                    
                    BufferedReader r = new BufferedReader(new InputStreamReader(p.getInputStream()));
                    String outLine;
                    while (true) {
                        outLine = r.readLine();
                        if (outLine == null) {
                            break;
                        }
                        System.out.println(outLine);
                    }
                    Logger.getLogger(this.getClass().getName()).log(Level.FINE, "System.out redirected");
                    System.out.println("System.out redirected");
                } catch (IOException ex) {
                    Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "Couldn't start OCEmu process", ex);
                    JOptionPane.showMessageDialog(null, ex, "Caught " + ex.getClass().getName(), JOptionPane.ERROR_MESSAGE);
                }
            }
        };
        // Starting the thread
        //t.start();
        System.out.println(OCEmuLauncher.runOCEmu());
        Logger.getLogger(this.getClass().getName()).log(Level.FINE, "OCEmu Thread started");
    }//GEN-LAST:event_launchButtonActionPerformed

    private void exitItemActionPerformed(ActionEvent evt) {//GEN-FIRST:event_exitItemActionPerformed
        this.dispose();
    }//GEN-LAST:event_exitItemActionPerformed

    private void importConfigItemActionPerformed(ActionEvent evt) {//GEN-FIRST:event_importConfigItemActionPerformed
        if (this.configChooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
            new OCEmuConfig(this.componentsArray).readConfig(this.configChooser.getSelectedFile());
            updateComponentList();
        }
    }//GEN-LAST:event_importConfigItemActionPerformed

    private void saveConfigItemActionPerformed(ActionEvent evt) {//GEN-FIRST:event_saveConfigItemActionPerformed
        new OCEmuConfig(this.componentsArray).createConfig();
    }//GEN-LAST:event_saveConfigItemActionPerformed

    private void componentTypeComboBoxActionPerformed(ActionEvent evt) {//GEN-FIRST:event_componentTypeComboBoxActionPerformed
        updateComponentList();
    }//GEN-LAST:event_componentTypeComboBoxActionPerformed

    private void resetConfigItemActionPerformed(ActionEvent evt) {//GEN-FIRST:event_resetConfigItemActionPerformed
        new OCEmuConfig(OCEmuLauncher.DEFAULT).createConfig();
        this.componentsArray = new OCEmuComponent[OCEmuLauncher.DEFAULT.length];
        System.arraycopy(OCEmuLauncher.DEFAULT, 0, this.componentsArray, 0, OCEmuLauncher.DEFAULT.length);
        updateComponentList();
    }//GEN-LAST:event_resetConfigItemActionPerformed

    private void jMenuItem1ActionPerformed(ActionEvent evt) {//GEN-FIRST:event_jMenuItem1ActionPerformed
        this.addComponent(new OCEmuComponent(0, UUIDGenerator.create(), ""));
    }//GEN-LAST:event_jMenuItem1ActionPerformed

    private void jMenuItem2ActionPerformed(ActionEvent evt) {//GEN-FIRST:event_jMenuItem2ActionPerformed
        if (this.componentList.getSelectedValue() != null) {
            deleteComponent(this.componentList.getSelectedIndex());
        } else {
            JOptionPane.showMessageDialog(this, "No component selected.", "Error.", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_jMenuItem2ActionPerformed

    JTextField componentAddressField;
    JTextArea componentDescriptionArea;
    JList<String> componentList;
    JComboBox<String> componentTypeComboBox;
    JFileChooser configChooser;
    JTextField option1Field;
    JTextField option2Field;
    JTextField option3Field;
    JTextField option4Field;
    // End of variables declaration//GEN-END:variables
}
