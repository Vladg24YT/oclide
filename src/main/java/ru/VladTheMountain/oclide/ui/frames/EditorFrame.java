/*
 * The MIT License
 *
 * Copyright 2021 Vladislav Gorskii.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package ru.VladTheMountain.oclide.ui.frames;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.util.Locale;
import java.util.Objects;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.accessibility.AccessibleContext;
import javax.swing.GroupLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.JToolBar;
import javax.swing.JTree;
import javax.swing.KeyStroke;
import javax.swing.LayoutStyle;
import javax.swing.SwingConstants;
import javax.swing.ToolTipManager;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.WindowConstants;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import org.apache.commons.io.FileUtils;
import org.fife.ui.autocomplete.AutoCompletion;
import org.fife.ui.autocomplete.CompletionCellRenderer;
import org.fife.ui.autocomplete.CompletionProvider;
import org.fife.ui.rsyntaxtextarea.FileLocation;
import org.fife.ui.rsyntaxtextarea.RSyntaxTextArea;
import org.fife.ui.rsyntaxtextarea.SyntaxConstants;
import org.fife.ui.rsyntaxtextarea.TextEditorPane;
import org.fife.ui.rtextarea.RTextScrollPane;
import org.fife.ui.rtextarea.ToolTipSupplier;
import ru.VladTheMountain.oclide.api.launcher.OCEmuLauncher;
import ru.VladTheMountain.oclide.ui.configurators.AurumEmulatorForm;
import ru.VladTheMountain.oclide.ui.configurators.CODEForm;
import ru.VladTheMountain.oclide.api.autocompletion.OCLIDECompletionProvider;
import ru.VladTheMountain.oclide.ui.configurators.OCEmuForm;
import ru.VladTheMountain.oclide.ui.dialogs.AboutDialog;
import ru.VladTheMountain.oclide.ui.dialogs.CreateNewProjectDialog;
import ru.VladTheMountain.oclide.ui.dialogs.OpenFileFileChooser;
import ru.VladTheMountain.oclide.ui.dialogs.ProjectFileChooser;
import ru.VladTheMountain.oclide.api.util.ConsoleOutputStream;

/**
 * Probably the most important and complicated class in the whole application.
 * It describes main Frame's UI, it's logic and sets {@link ActionListener}s for
 * almost every dialog present in {@code ru.VladTheMountain.oclide.ui.dialogs}
 *
 * @author VladTheMountain
 */
public class EditorFrame extends JFrame {

    final static ResourceBundle localizationResource = ResourceBundle.getBundle("editor.Editor", Locale.getDefault());

    /**
     * Creates new form MainFrame
     */
    public EditorFrame() {
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException ex) {
            Logger.getLogger(EditorFrame.class.getName()).log(Level.SEVERE, "Error while configuring L&F: ", ex);
            JOptionPane.showMessageDialog(this, ex, "Caught " + ex.getClass().getName(), JOptionPane.ERROR_MESSAGE);
        }
        initComponents();
        System.setOut(new PrintStream(new ConsoleOutputStream(outputTextArea)));
    }

    /**
     * Creates a new file and opens it in a new {@link RSyntaxTextArea}
     *
     * @param file A file to save contents to
     */
    private void newFile(File file) {
        try {
            file.createNewFile();
        } catch (IOException ex) {
            Logger.getLogger(EditorFrame.class.getName()).log(Level.SEVERE, "Error while creating a new file: ", ex);
            JOptionPane.showMessageDialog(this, ex, "Caught " + ex.getClass().getName(), JOptionPane.ERROR_MESSAGE);
        }
        try {
            openFile(file); //:D
        } catch (FileNotFoundException ex) {
            Logger.getLogger(EditorFrame.class.getName()).log(Level.SEVERE, "File not found", ex);
        }
    }

    /**
     * Reads the contents of {@code file} into a new {@link RSyntaxTextArea} in
     * a new tab
     *
     * @param file the {@link File}, which contents are to read
     */
    private void openFile(File file) throws FileNotFoundException {
        if (!(file.exists())) {
            JOptionPane.showMessageDialog(this, "The requested file " + file.getAbsolutePath() + " does not exist.", "File does not exist", JOptionPane.ERROR_MESSAGE);
            throw new FileNotFoundException("The requested file does not exist");
        }
        TextEditorPane newFile = null/*RSyntaxTextArea(fileContent)*/;
        try {
            newFile = new TextEditorPane(TextEditorPane.INSERT_MODE, false, FileLocation.create(file))/*RSyntaxTextArea(fileContent)*/;
        } catch (IOException ex) {
            Logger.getLogger(EditorFrame.class.getName()).log(Level.SEVERE, null, ex);
        }
        Objects.requireNonNull(newFile).setSyntaxEditingStyle(SyntaxConstants.SYNTAX_STYLE_LUA);
        newFile.setCodeFoldingEnabled(true);
        newFile.getAccessibleContext().setAccessibleDescription(newFile.getText());
        //autocompletion
        CompletionProvider occp = OCLIDECompletionProvider.getProvider();
        AutoCompletion occ = new AutoCompletion(occp);
        occ.setListCellRenderer(new CompletionCellRenderer());
        occ.setAutoCompleteEnabled(true);
        occ.setAutoActivationEnabled(true);
        occ.setAutoActivationDelay(100);
        occ.setShowDescWindow(true);
        occ.setParameterAssistanceEnabled(true);
        occ.install(newFile);
        newFile.setToolTipSupplier((ToolTipSupplier) occp);
        ToolTipManager.sharedInstance().registerComponent(newFile);
        //
        RTextScrollPane sp = new RTextScrollPane(newFile);
        sp.setName(file.getAbsolutePath());
        this.editorTabs.add(file.getName(), sp);
        this.editorTabs.setSelectedIndex(this.editorTabs.getTabCount() - 1);
        updateProjectsTree();
    }

    /**
     * Saves {@link RSyntaxTextArea} contents to the file
     *
     * @param f The {@link File} to save current {@link RSyntaxTextArea}
     * contents to
     */
    private void saveFile(File f) {
        if (!(f.exists())) {
            f.mkdirs();
            try {
                f.createNewFile();
            } catch (IOException ex) {
                Logger.getLogger(EditorFrame.class.getName()).log(Level.SEVERE, "Error while creating new file: ", ex);
                JOptionPane.showMessageDialog(null, ex, "Caught " + ex.getClass().getName(), JOptionPane.ERROR_MESSAGE);
            }
        }
        try {
            Files.write(f.toPath(), this.editorTabs.getComponentAt(this.editorTabs.getSelectedIndex()).getAccessibleContext().getAccessibleChild(0).getAccessibleContext().getAccessibleChild(0).getAccessibleContext().getAccessibleDescription().getBytes());
        } catch (IOException ex) {
            Logger.getLogger(EditorFrame.class.getName()).log(Level.SEVERE, "Error while writing into file: ", ex);
            JOptionPane.showMessageDialog(null, ex, "Caught " + ex.getClass().getName(), JOptionPane.ERROR_MESSAGE);
        }
        updateProjectsTree();
    }

    /**
     * Creates a new project and adds it to the projects' {@link JTree}
     *
     * @param name Project's name
     * @param projectDir Path to project's files
     */
    public static void createProject(String name, String projectDir) {
        File proj = new File(projectDir);
        if (!(proj.exists())) {
            proj.mkdirs();
        } else {
            JOptionPane.showMessageDialog(null, "Such directory already exists", "Wrong project name", JOptionPane.ERROR_MESSAGE);
        }
        updateProjectsTree();
    }

    /**
     * Refreshes the {@code projectsTree} {@link JTree}
     */
    private static void updateProjectsTree() {
        //Tree values
        DefaultMutableTreeNode rootNode = new DefaultMutableTreeNode(localizationResource.getString("treeProjects"));
        DefaultTreeModel projTreeModel = new DefaultTreeModel(rootNode);
        //Files' bullsh*t
        File projectsDir = new File("projects");
        if (projectsDir.exists()) {
            recursivelyAddFiles(projectsDir, rootNode);
        }
        //Refreshing the JTree itself
        projectsTree.setModel(projTreeModel);
    }

    /**
     * Recursively gets all files in the project folder
     *
     * @param file Project folder as a {@link File}
     * @param n Project's tree node
     */
    private static void recursivelyAddFiles(File file, DefaultMutableTreeNode n) {
        File[] files = file.listFiles();
        if (files == null) {
            return;
        }
        for (File f : files) {
            DefaultMutableTreeNode cNode = new DefaultMutableTreeNode(f.getName());
            n.add(cNode);
            if (f.isDirectory()) {
                recursivelyAddFiles(f, cNode);
            }
        }
    }

    /**
     * Refreshes the {@code variableTree} {@link JTree}
     */
    private void updateVariableTree() {
        DefaultMutableTreeNode rootNode = new DefaultMutableTreeNode(this.editorTabs.getTitleAt(this.editorTabs.getSelectedIndex()));
        DefaultTreeModel model = new DefaultTreeModel(rootNode);
        //

        //
        this.variableTree.setModel(model);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        fileManagementPopup = new JPopupMenu();
        JMenuItem popupSaveFile = new JMenuItem();
        JMenuItem popupCloseFile = new JMenuItem();
        projectManagementPopup = new JPopupMenu();
        JMenu newMenu = new JMenu();
        JMenuItem addFileMenuItem = new JMenuItem();
        JMenuItem addFolderMenuItem = new JMenuItem();
        JPopupMenu.Separator jSeparator7 = new JPopupMenu.Separator();
        JMenuItem runInOCEmu = new JMenuItem();
        JPopupMenu.Separator jSeparator8 = new JPopupMenu.Separator();
        JMenuItem renameMenuItem = new JMenuItem();
        JMenuItem deleteMenuItem = new JMenuItem();
        JPopupMenu.Separator jSeparator9 = new JPopupMenu.Separator();
        JMenuItem propertiesMenuItem = new JMenuItem();
        JFileChooser projectChooser = new JFileChooser();
        JSplitPane jSplitPane1 = new JSplitPane();
        JSplitPane jSplitPane3 = new JSplitPane();
        editorTabs = new JTabbedPane();
        JTabbedPane jTabbedPane1 = new JTabbedPane();
        JScrollPane jScrollPane2 = new JScrollPane();
        outputTextArea = new JTextArea();
        JSplitPane jSplitPane2 = new JSplitPane();
        JScrollPane jScrollPane1 = new JScrollPane();
        variableTree = new JTree();
        JScrollPane projectsScroll = new JScrollPane();
        projectsTree = new JTree();
        JToolBar projectToolbar = new JToolBar();
        JButton newProjectButton = new JButton();
        JButton openProjectButton = new JButton();
        JButton addFileButton = new JButton();
        JButton saveButton = new JButton();
        JButton deleteProjectButton = new JButton();
        JToolBar undoRedoToolbar = new JToolBar();
        JButton undoButton = new JButton();
        JButton redoButton = new JButton();
        JToolBar jToolBar1 = new JToolBar();
        JButton runOCEmuButton = new JButton();
        JButton runOcelotButton = new JButton();
        JMenuBar menuBar = new JMenuBar();
        JMenu fileMenu = new JMenu();
        JMenuItem createProject = new JMenuItem();
        JMenuItem openProject = new JMenuItem();
        JPopupMenu.Separator jSeparator1 = new JPopupMenu.Separator();
        JMenuItem createFile = new JMenuItem();
        JMenuItem openFile = new JMenuItem();
        JPopupMenu.Separator jSeparator10 = new JPopupMenu.Separator();
        JMenuItem save = new JMenuItem();
        JPopupMenu.Separator jSeparator6 = new JPopupMenu.Separator();
        JMenuItem deleteProject = new JMenuItem();
        JPopupMenu.Separator jSeparator2 = new JPopupMenu.Separator();
        JMenuItem exit = new JMenuItem();
        JMenu editMenu = new JMenu();
        JMenuItem undo = new JMenuItem();
        JMenuItem redo = new JMenuItem();
        JPopupMenu.Separator jSeparator3 = new JPopupMenu.Separator();
        JMenuItem cut = new JMenuItem();
        JMenuItem copy = new JMenuItem();
        JMenuItem paste = new JMenuItem();
        JPopupMenu.Separator jSeparator4 = new JPopupMenu.Separator();
        JMenuItem find = new JMenuItem();
        JMenu runMenu = new JMenu();
        JMenu emulatorMenu = new JMenu();
        JMenuItem ocemuMenuItem = new JMenuItem();
        JMenuItem ocelotMenuItem = new JMenuItem();
        JMenuItem ocemulatorMenuItem = new JMenuItem();
        JMenuItem aurumMenuItem = new JMenuItem();
        JMenuItem codeMenuItem = new JMenuItem();
        JMenu vmMenu = new JMenu();
        JMenuItem ocvmMenuItem = new JMenuItem();
        JMenuItem ocvmFXMenuItem = new JMenuItem();
        JPopupMenu.Separator jSeparator5 = new JPopupMenu.Separator();
        JMenuItem jMenuItem1 = new JMenuItem();
        JMenu helpMenu = new JMenu();
        // Variables declaration - do not modify//GEN-BEGIN:variables
        JMenuItem aboutMenuItem = new JMenuItem();
        JMenuItem wikiMenuItem = new JMenuItem();
        JPopupMenu.Separator jSeparator11 = new JPopupMenu.Separator();
        JMenuItem settingsMenuItem = new JMenuItem();

        popupSaveFile.setIcon(new ImageIcon(Objects.requireNonNull(getClass().getResource("/assets/icons/save_icon&16.png")))); // NOI18N
        popupSaveFile.setText("Save current file");
        popupSaveFile.addActionListener(this::popupSaveFileActionPerformed);
        fileManagementPopup.add(popupSaveFile);

        popupCloseFile.setIcon(new ImageIcon(Objects.requireNonNull(getClass().getResource("/assets/icons/doc_delete_icon&16.png")))); // NOI18N
        popupCloseFile.setText("Close current file");
        popupCloseFile.addActionListener(this::popupCloseFileActionPerformed);
        fileManagementPopup.add(popupCloseFile);

        newMenu.setText("Add...");
        newMenu.setToolTipText("");

        addFileMenuItem.setIcon(new ImageIcon(Objects.requireNonNull(getClass().getResource("/assets/icons/document_icon&16.png")))); // NOI18N
        addFileMenuItem.setText("File");
        addFileMenuItem.addActionListener(this::addFileMenuItemActionPerformed);
        newMenu.add(addFileMenuItem);

        addFolderMenuItem.setIcon(new ImageIcon(Objects.requireNonNull(getClass().getResource("/assets/icons/folder_icon&16.png")))); // NOI18N
        addFolderMenuItem.setText("Folder");
        addFolderMenuItem.addActionListener(this::addFolderMenuItemActionPerformed);
        newMenu.add(addFolderMenuItem);

        projectManagementPopup.add(newMenu);
        projectManagementPopup.add(jSeparator7);

        runInOCEmu.setIcon(new ImageIcon(Objects.requireNonNull(getClass().getResource("/assets/icons/app_window_shell&16.png")))); // NOI18N
        runInOCEmu.setText("Launch OCEmu");
        runInOCEmu.setToolTipText("");
        runInOCEmu.addActionListener(this::runInOCEmuActionPerformed);
        projectManagementPopup.add(runInOCEmu);
        projectManagementPopup.add(jSeparator8);

        renameMenuItem.setText("Rename");
        renameMenuItem.setToolTipText("");
        renameMenuItem.addActionListener(this::renameMenuItemActionPerformed);
        projectManagementPopup.add(renameMenuItem);

        deleteMenuItem.setIcon(new ImageIcon(Objects.requireNonNull(getClass().getResource("/assets/icons/folder_delete_icon&16.png")))); // NOI18N
        deleteMenuItem.setText("Delete");
        deleteMenuItem.addActionListener(this::deleteMenuItemActionPerformed);
        projectManagementPopup.add(deleteMenuItem);
        projectManagementPopup.add(jSeparator9);

        propertiesMenuItem.setIcon(new ImageIcon(Objects.requireNonNull(getClass().getResource("/assets/icons/cogs_icon&16.png")))); // NOI18N
        propertiesMenuItem.setText("Properties");
        propertiesMenuItem.setToolTipText("");
        propertiesMenuItem.addActionListener(this::propertiesMenuItemActionPerformed);
        projectManagementPopup.add(propertiesMenuItem);

        projectChooser.setCurrentDirectory(new File(System.getProperty("user.home")));
        projectChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);

        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setTitle("OCLIDE - OpenComputers Lua Integrated Development Environment (beta v0.1.0)");
        setExtendedState(JFrame.MAXIMIZED_BOTH);

        jSplitPane3.setOrientation(JSplitPane.VERTICAL_SPLIT);
        jSplitPane3.setResizeWeight(1.0);

        editorTabs.setPreferredSize(getMaximumSize());
        editorTabs.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent evt) {
                editorTabsMouseClicked(evt);
            }
        });
        jSplitPane3.setLeftComponent(editorTabs);

        outputTextArea.setEditable(false);
        outputTextArea.setColumns(20);
        outputTextArea.setFont(new Font("Lucida Console", Font.PLAIN, 12)); // NOI18N
        outputTextArea.setRows(5);
        jScrollPane2.setViewportView(outputTextArea);

        jTabbedPane1.addTab(localizationResource.getString("tabIDEOutput"), jScrollPane2);

        jSplitPane3.setBottomComponent(jTabbedPane1);

        jSplitPane1.setRightComponent(jSplitPane3);

        jSplitPane2.setOrientation(JSplitPane.VERTICAL_SPLIT);
        jSplitPane2.setResizeWeight(1.0);

        jScrollPane1.setEnabled(false);

        variableTree.setModel(new DefaultTreeModel(new DefaultMutableTreeNode(
            this.editorTabs.getSelectedIndex()==-1 ?
            "Variables"
            : this.editorTabs.getTitleAt(this.editorTabs.getSelectedIndex()))));
variableTree.setEnabled(false);
jScrollPane1.setViewportView(variableTree);

jSplitPane2.setBottomComponent(jScrollPane1);

        DefaultMutableTreeNode treeNode1 = new DefaultMutableTreeNode("Projects");
projectsTree.setModel(new DefaultTreeModel(treeNode1));
projectsTree.addMouseListener(new MouseAdapter() {
    public void mouseClicked(MouseEvent evt) {
        projectsTreeMouseClicked(evt);
    }
    });
    projectsScroll.setViewportView(projectsTree);
    updateProjectsTree();

    jSplitPane2.setLeftComponent(projectsScroll);

    jSplitPane1.setLeftComponent(jSplitPane2);

    projectToolbar.setFloatable(false);
    projectToolbar.setRollover(true);

    newProjectButton.setIcon(new ImageIcon(Objects.requireNonNull(getClass().getResource("/assets/icons/folder_plus_icon&24.png")))); // NOI18N
    newProjectButton.setToolTipText(localizationResource.getString("tooltipCreateProject"));
    newProjectButton.setFocusable(false);
    newProjectButton.setHorizontalTextPosition(SwingConstants.CENTER);
    newProjectButton.setVerticalTextPosition(SwingConstants.BOTTOM);
    newProjectButton.addActionListener(this::newProjectButtonActionPerformed);
    projectToolbar.add(newProjectButton);

    openProjectButton.setIcon(new ImageIcon(Objects.requireNonNull(getClass().getResource("/assets/icons/folder_open_icon&24.png")))); // NOI18N
    openProjectButton.setToolTipText(localizationResource.getString("tooltipOpenProject"));
    openProjectButton.setFocusable(false);
    openProjectButton.setHorizontalTextPosition(SwingConstants.CENTER);
    openProjectButton.setVerticalTextPosition(SwingConstants.BOTTOM);
    openProjectButton.addActionListener(this::openProjectButtonActionPerformed);
    projectToolbar.add(openProjectButton);

    addFileButton.setIcon(new ImageIcon(Objects.requireNonNull(getClass().getResource("/assets/icons/doc_plus_icon&24.png")))); // NOI18N
    addFileButton.setToolTipText(localizationResource.getString("tooltipCreateFile"));
    addFileButton.setFocusable(false);
    addFileButton.setHorizontalTextPosition(SwingConstants.CENTER);
    addFileButton.setVerticalTextPosition(SwingConstants.BOTTOM);
    addFileButton.addActionListener(this::addFileButtonActionPerformed);
    projectToolbar.add(addFileButton);

    saveButton.setIcon(new ImageIcon(Objects.requireNonNull(getClass().getResource("/assets/icons/save_icon&24.png")))); // NOI18N
    saveButton.setToolTipText(localizationResource.getString("tooltipSaveFile"));
    saveButton.setFocusable(false);
    saveButton.setHorizontalTextPosition(SwingConstants.CENTER);
    saveButton.setVerticalTextPosition(SwingConstants.BOTTOM);
    saveButton.addActionListener(this::saveButtonActionPerformed);
    projectToolbar.add(saveButton);

    deleteProjectButton.setIcon(new ImageIcon(Objects.requireNonNull(getClass().getResource("/assets/icons/folder_delete_icon&24.png")))); // NOI18N
    deleteProjectButton.setToolTipText(localizationResource.getString("tooltipDeleteProject"));
    deleteProjectButton.setFocusable(false);
    deleteProjectButton.setHorizontalTextPosition(SwingConstants.CENTER);
    deleteProjectButton.setVerticalTextPosition(SwingConstants.BOTTOM);
    deleteProjectButton.addActionListener(this::deleteProjectButtonActionPerformed);
    projectToolbar.add(deleteProjectButton);

    undoRedoToolbar.setFloatable(false);
    undoRedoToolbar.setRollover(true);

    undoButton.setIcon(new ImageIcon(Objects.requireNonNull(getClass().getResource("/assets/icons/undo_icon&24.png")))); // NOI18N
    undoButton.setToolTipText(localizationResource.getString("tooltipUndo"));
    undoButton.setFocusable(false);
    undoButton.setHorizontalTextPosition(SwingConstants.CENTER);
    undoButton.setVerticalTextPosition(SwingConstants.BOTTOM);
    undoRedoToolbar.add(undoButton);

    redoButton.setIcon(new ImageIcon(Objects.requireNonNull(getClass().getResource("/assets/icons/redo_icon&24.png")))); // NOI18N
    redoButton.setToolTipText(localizationResource.getString("tooltipRedo"));
    redoButton.setFocusable(false);
    redoButton.setHorizontalTextPosition(SwingConstants.CENTER);
    redoButton.setVerticalTextPosition(SwingConstants.BOTTOM);
    undoRedoToolbar.add(redoButton);

    jToolBar1.setFloatable(false);
    jToolBar1.setRollover(true);

    runOCEmuButton.setIcon(new ImageIcon(Objects.requireNonNull(getClass().getResource("/assets/icons/app_window_shell&24.png")))); // NOI18N
    runOCEmuButton.setToolTipText(localizationResource.getString("tooltipOCEmu"));
    runOCEmuButton.setFocusable(false);
    runOCEmuButton.setHorizontalTextPosition(SwingConstants.CENTER);
    runOCEmuButton.setVerticalTextPosition(SwingConstants.BOTTOM);
    runOCEmuButton.addActionListener(this::runOCEmuButtonActionPerformed);
    jToolBar1.add(runOCEmuButton);

    runOcelotButton.setIcon(new ImageIcon(Objects.requireNonNull(getClass().getResource("/assets/icons/app_window_black&24.png")))); // NOI18N
    runOcelotButton.setToolTipText(localizationResource.getString("tooltipOcelot"));
    runOcelotButton.setVerticalTextPosition(SwingConstants.BOTTOM);
    runOcelotButton.addActionListener(this::runOcelotButtonActionPerformed);
    jToolBar1.add(runOcelotButton);

    fileMenu.setText(localizationResource.getString("menuFile"));

    createProject.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_N, InputEvent.CTRL_DOWN_MASK));
    createProject.setIcon(new ImageIcon(Objects.requireNonNull(getClass().getResource("/assets/icons/folder_plus_icon&16.png")))); // NOI18N
    createProject.setText(localizationResource.getString("menuItemCreateProject"));
    createProject.addActionListener(this::createProjectActionPerformed);
    fileMenu.add(createProject);

    openProject.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_O, InputEvent.CTRL_DOWN_MASK));
    openProject.setIcon(new ImageIcon(Objects.requireNonNull(getClass().getResource("/assets/icons/folder_open_icon&16.png")))); // NOI18N
    openProject.setText(localizationResource.getString("menuItemOpenProject"));
    openProject.addActionListener(this::openProjectActionPerformed);
    fileMenu.add(openProject);
    fileMenu.add(jSeparator1);

    createFile.setIcon(new ImageIcon(Objects.requireNonNull(getClass().getResource("/assets/icons/doc_plus_icon&16.png")))); // NOI18N
    createFile.setText(localizationResource.getString("menuItemCreateFile"));
    createFile.addActionListener(this::createFileActionPerformed);
    fileMenu.add(createFile);

    openFile.setIcon(new ImageIcon(Objects.requireNonNull(getClass().getResource("/assets/icons/document_icon&16.png")))); // NOI18N
    openFile.setText(localizationResource.getString("menuItemOpenFile"));
    openFile.addActionListener(this::openFileActionPerformed);
    fileMenu.add(openFile);
    fileMenu.add(jSeparator10);

    save.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, InputEvent.CTRL_DOWN_MASK));
    save.setIcon(new ImageIcon(Objects.requireNonNull(getClass().getResource("/assets/icons/save_icon&16.png")))); // NOI18N
    save.setText(localizationResource.getString("menuItemSaveFile"));
    save.addActionListener(this::saveActionPerformed);
    fileMenu.add(save);
    fileMenu.add(jSeparator6);

    deleteProject.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_DELETE, InputEvent.CTRL_DOWN_MASK));
    deleteProject.setIcon(new ImageIcon(Objects.requireNonNull(getClass().getResource("/assets/icons/folder_delete_icon&16.png")))); // NOI18N
    deleteProject.setText(localizationResource.getString("menuItemDeleteProject"));
    deleteProject.addActionListener(this::deleteProjectActionPerformed);
    fileMenu.add(deleteProject);
    fileMenu.add(jSeparator2);

    exit.setText(localizationResource.getString("menuItemExit"));
    exit.addActionListener(this::exitActionPerformed);
    fileMenu.add(exit);

    menuBar.add(fileMenu);

    editMenu.setText(localizationResource.getString("menuEdit"));

    undo.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Z, InputEvent.CTRL_DOWN_MASK));
    undo.setIcon(new ImageIcon(Objects.requireNonNull(getClass().getResource("/assets/icons/undo_icon&16.png")))); // NOI18N
    undo.setText(localizationResource.getString("menuItemUndo"));
    editMenu.add(undo);

    redo.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Y, InputEvent.CTRL_DOWN_MASK));
    redo.setIcon(new ImageIcon(Objects.requireNonNull(getClass().getResource("/assets/icons/redo_icon&16.png")))); // NOI18N
    redo.setText(localizationResource.getString("menuItemRedo"));
    editMenu.add(redo);
    editMenu.add(jSeparator3);

    cut.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_X, InputEvent.CTRL_DOWN_MASK));
    cut.setIcon(new ImageIcon(Objects.requireNonNull(getClass().getResource("/assets/icons/clipboard_cut_icon&16.png")))); // NOI18N
    cut.setText(localizationResource.getString("menuItemCut"));
    cut.addActionListener(this::cutActionPerformed);
    editMenu.add(cut);

    copy.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_C, InputEvent.CTRL_DOWN_MASK));
    copy.setIcon(new ImageIcon(Objects.requireNonNull(getClass().getResource("/assets/icons/clipboard_copy_icon&16.png")))); // NOI18N
    copy.setText(localizationResource.getString("menuItemCopy"));
    copy.addActionListener(this::copyActionPerformed);
    editMenu.add(copy);

    paste.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_V, InputEvent.CTRL_DOWN_MASK));
    paste.setIcon(new ImageIcon(Objects.requireNonNull(getClass().getResource("/assets/icons/clipboard_past_icon&16.png")))); // NOI18N
    paste.setText(localizationResource.getString("menuItemPaste"));
    paste.addActionListener(this::pasteActionPerformed);
    editMenu.add(paste);
    editMenu.add(jSeparator4);

    find.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F, InputEvent.CTRL_DOWN_MASK));
    find.setText(localizationResource.getString("menuItemFind"));
    find.addActionListener(this::findActionPerformed);
    editMenu.add(find);

    menuBar.add(editMenu);

    runMenu.setText(localizationResource.getString("menuDeploy"));

    emulatorMenu.setText(localizationResource.getString("menuItemDeployToEmulator"));

    ocemuMenuItem.setIcon(new ImageIcon(Objects.requireNonNull(getClass().getResource("/assets/icons/app_window_shell&16.png")))); // NOI18N
    ocemuMenuItem.setText(localizationResource.getString("menuItemOCEmu"));
    ocemuMenuItem.addActionListener(this::ocemuMenuItemActionPerformed);
    emulatorMenu.add(ocemuMenuItem);

    ocelotMenuItem.setIcon(new ImageIcon(Objects.requireNonNull(getClass().getResource("/assets/icons/app_window_black&16.png")))); // NOI18N
    ocelotMenuItem.setText(localizationResource.getString("menuItemOcelot"));
    ocelotMenuItem.addActionListener(this::ocelotMenuItemActionPerformed);
    emulatorMenu.add(ocelotMenuItem);

    ocemulatorMenuItem.setText(localizationResource.getString("menuItemOCEmulator"));
    ocemulatorMenuItem.setEnabled(false);
    ocemulatorMenuItem.addActionListener(this::ocemulatorMenuItemActionPerformed);
    emulatorMenu.add(ocemulatorMenuItem);

    aurumMenuItem.setText(localizationResource.getString("menuItemAurum"));
    aurumMenuItem.setEnabled(false);
    aurumMenuItem.addActionListener(this::aurumMenuItemActionPerformed);
    emulatorMenu.add(aurumMenuItem);

    codeMenuItem.setText(localizationResource.getString("menuItemCODE"));
    codeMenuItem.setEnabled(false);
    codeMenuItem.addActionListener(this::codeMenuItemActionPerformed);
    emulatorMenu.add(codeMenuItem);

    runMenu.add(emulatorMenu);

    vmMenu.setText(localizationResource.getString("menuItemDeployToVM"));

    ocvmMenuItem.setText(localizationResource.getString("menuItemOCVM"));
    ocvmMenuItem.setEnabled(false);
    vmMenu.add(ocvmMenuItem);

    ocvmFXMenuItem.setText(localizationResource.getString("menuItemOpenComputersVM"));
    ocvmFXMenuItem.setEnabled(false);
    vmMenu.add(ocvmFXMenuItem);

    runMenu.add(vmMenu);
    runMenu.add(jSeparator5);

    jMenuItem1.setText(localizationResource.getString("menuItemDeployToMinecraft"));
    jMenuItem1.setEnabled(false);
    runMenu.add(jMenuItem1);

    menuBar.add(runMenu);

    helpMenu.setText(localizationResource.getString("menuHelp"));

    aboutMenuItem.setText(localizationResource.getString("menuItemAbout"));
    aboutMenuItem.addActionListener(this::aboutMenuItemActionPerformed);
    helpMenu.add(aboutMenuItem);

    wikiMenuItem.setText(localizationResource.getString("menuItemWiki"));
    wikiMenuItem.setEnabled(false);
    helpMenu.add(wikiMenuItem);
    helpMenu.add(jSeparator11);

    settingsMenuItem.setIcon(new ImageIcon(Objects.requireNonNull(getClass().getResource("/assets/icons/cogs_icon&16.png")))); // NOI18N
    settingsMenuItem.setText(localizationResource.getString("menuItemSettings"));
    settingsMenuItem.addActionListener(this::settingsMenuItemActionPerformed);
    helpMenu.add(settingsMenuItem);

    menuBar.add(helpMenu);

    setJMenuBar(menuBar);

        GroupLayout layout = new GroupLayout(getContentPane());
    getContentPane().setLayout(layout);
    layout.setHorizontalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
        .addGroup(layout.createSequentialGroup()
            .addComponent(projectToolbar, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
            .addComponent(undoRedoToolbar, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
            .addComponent(jToolBar1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
            .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        .addComponent(jSplitPane1)
    );
    layout.setVerticalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
        .addGroup(GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
            .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING, false)
                .addComponent(undoRedoToolbar, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(projectToolbar, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jToolBar1, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
            .addComponent(jSplitPane1, GroupLayout.DEFAULT_SIZE, 464, Short.MAX_VALUE)
            .addContainerGap())
    );

    pack();
    setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void ocemuMenuItemActionPerformed(ActionEvent evt) {//GEN-FIRST:event_ocemuMenuItemActionPerformed
        runOCEmu();
    }//GEN-LAST:event_ocemuMenuItemActionPerformed

    private void aboutMenuItemActionPerformed(ActionEvent evt) {//GEN-FIRST:event_aboutMenuItemActionPerformed
        new AboutDialog(this);
    }//GEN-LAST:event_aboutMenuItemActionPerformed

    private void createProjectActionPerformed(ActionEvent evt) {//GEN-FIRST:event_createProjectActionPerformed
        new CreateNewProjectDialog(this).setVisible(true);
    }//GEN-LAST:event_createProjectActionPerformed

    private void ocelotMenuItemActionPerformed(ActionEvent evt) {//GEN-FIRST:event_ocelotMenuItemActionPerformed
        runOcelot();
    }//GEN-LAST:event_ocelotMenuItemActionPerformed

    private void exitActionPerformed(ActionEvent evt) {//GEN-FIRST:event_exitActionPerformed
        this.saveActionPerformed(evt);
        System.exit(0);
    }//GEN-LAST:event_exitActionPerformed

    private void deleteProjectActionPerformed(ActionEvent evt) {//GEN-FIRST:event_deleteProjectActionPerformed
        int result = JOptionPane.showConfirmDialog(this, "Are you really want to delete your project?", "Deleting a project", JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE);
        if (result == JOptionPane.YES_OPTION) {
            String projectName = String.valueOf(Objects.requireNonNull(projectsTree.getSelectionPath()).getPath()[1]);
            File f = new File("projects/" + projectName);
            if (f.delete()) {
                JOptionPane.showMessageDialog(this, "Project successfully deleted.", "Deleting a project", JOptionPane.INFORMATION_MESSAGE);
            }
        }
    }//GEN-LAST:event_deleteProjectActionPerformed

    private void saveActionPerformed(ActionEvent evt) {//GEN-FIRST:event_saveActionPerformed
        this.saveFile(new File(this.editorTabs.getComponentAt(this.editorTabs.getSelectedIndex()).getName()));
    }//GEN-LAST:event_saveActionPerformed

    private void openProjectActionPerformed(ActionEvent evt) {//GEN-FIRST:event_openProjectActionPerformed
        JFileChooser projectChoooser = new ProjectFileChooser();
        if (projectChoooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
            File dir = projectChoooser.getSelectedFile();
            try {
                FileUtils.copyDirectory(dir, new File("projects"));
            } catch (IOException ex) {
                Logger.getLogger(EditorFrame.class.getName()).log(Level.SEVERE, null, ex);
                JOptionPane.showMessageDialog(null, ex, "Caught " + ex.getClass().getName(), JOptionPane.ERROR_MESSAGE);
            }
            updateProjectsTree();
        }
    }//GEN-LAST:event_openProjectActionPerformed

    private void cutActionPerformed(ActionEvent evt) {//GEN-FIRST:event_cutActionPerformed
        AccessibleContext ac = this.editorTabs.getComponentAt(this.editorTabs.getSelectedIndex()).getAccessibleContext().getAccessibleChild(0).getAccessibleContext().getAccessibleChild(0).getAccessibleContext();
        ac.getAccessibleEditableText().cut(ac.getAccessibleEditableText().getSelectionStart(), ac.getAccessibleEditableText().getSelectionEnd());
    }//GEN-LAST:event_cutActionPerformed

    private void copyActionPerformed(ActionEvent evt) {//GEN-FIRST:event_copyActionPerformed
        AccessibleContext ac = this.editorTabs.getComponentAt(this.editorTabs.getSelectedIndex()).getAccessibleContext().getAccessibleChild(0).getAccessibleContext().getAccessibleChild(0).getAccessibleContext();
        ac.getAccessibleEditableText().getTextRange(ac.getAccessibleEditableText().getSelectionStart(), ac.getAccessibleEditableText().getSelectionEnd());
    }//GEN-LAST:event_copyActionPerformed

    private void pasteActionPerformed(ActionEvent evt) {//GEN-FIRST:event_pasteActionPerformed
        AccessibleContext ac = this.editorTabs.getComponentAt(this.editorTabs.getSelectedIndex()).getAccessibleContext().getAccessibleChild(0).getAccessibleContext().getAccessibleChild(0).getAccessibleContext();
        ac.getAccessibleEditableText().paste(ac.getAccessibleEditableText().getCaretPosition());
    }//GEN-LAST:event_pasteActionPerformed

    private void findActionPerformed(ActionEvent evt) {//GEN-FIRST:event_findActionPerformed
        JOptionPane.showMessageDialog(this, "Unimplemented feature. Wait for next updates");
    }//GEN-LAST:event_findActionPerformed

    private void popupSaveFileActionPerformed(ActionEvent evt) {//GEN-FIRST:event_popupSaveFileActionPerformed
        this.saveActionPerformed(evt);
    }//GEN-LAST:event_popupSaveFileActionPerformed

    private void popupCloseFileActionPerformed(ActionEvent evt) {//GEN-FIRST:event_popupCloseFileActionPerformed
        this.editorTabs.remove(this.editorTabs.getSelectedIndex());
    }//GEN-LAST:event_popupCloseFileActionPerformed

    private void editorTabsMouseClicked(MouseEvent evt) {//GEN-FIRST:event_editorTabsMouseClicked
        if (evt.getButton() == MouseEvent.BUTTON3) {
            this.fileManagementPopup.show(this.editorTabs, evt.getX(), evt.getY());
        }
    }//GEN-LAST:event_editorTabsMouseClicked

    private void projectsTreeMouseClicked(MouseEvent evt) {//GEN-FIRST:event_projectsTreeMouseClicked
        if (evt.getButton() == MouseEvent.BUTTON3 && Objects.requireNonNull(projectsTree.getSelectionPath()).getParentPath() == projectsTree.getPathForRow(0)) {
            this.projectManagementPopup.show(projectsTree, evt.getX(), evt.getY());
        } else if (projectsTree.getSelectionCount() != 0 && FileSystems.getDefault().getPath("", Objects.requireNonNull(projectsTree.getSelectionPath()).toString().substring(1, projectsTree.getSelectionPath().toString().length() - 1).split(", ")).toFile().isFile() && evt.getButton() == MouseEvent.BUTTON1 && evt.getClickCount() == 2) {
            try {
                this.openFile(FileSystems.getDefault().getPath("", projectsTree.getSelectionPath().toString().substring(1, projectsTree.getSelectionPath().toString().length() - 1).split(", ")).toFile());
            } catch (FileNotFoundException ex) {
                Logger.getLogger(EditorFrame.class.getName()).log(Level.SEVERE, "File not found", ex);
            }
        }
    }//GEN-LAST:event_projectsTreeMouseClicked

    private void runInOCEmuActionPerformed(ActionEvent evt) {//GEN-FIRST:event_runInOCEmuActionPerformed
        runOCEmu();
    }//GEN-LAST:event_runInOCEmuActionPerformed

    private void newProjectButtonActionPerformed(ActionEvent evt) {//GEN-FIRST:event_newProjectButtonActionPerformed
        new CreateNewProjectDialog(this).setVisible(true);
    }//GEN-LAST:event_newProjectButtonActionPerformed

    private void openProjectButtonActionPerformed(ActionEvent evt) {//GEN-FIRST:event_openProjectButtonActionPerformed
        this.openProjectActionPerformed(evt);
    }//GEN-LAST:event_openProjectButtonActionPerformed

    private void addFileButtonActionPerformed(ActionEvent evt) {//GEN-FIRST:event_addFileButtonActionPerformed
        this.addFileMenuItemActionPerformed(evt);
    }//GEN-LAST:event_addFileButtonActionPerformed

    private void addFileMenuItemActionPerformed(ActionEvent evt) {//GEN-FIRST:event_addFileMenuItemActionPerformed
        String name = JOptionPane.showInputDialog(this, "Name a new file:", "");
        if (name != null) {
            this.newFile(new File("projects/" + Objects.requireNonNull(projectsTree.getSelectionPath()).getPath()[1] + FileSystems.getDefault().getSeparator() + name));
        }
    }//GEN-LAST:event_addFileMenuItemActionPerformed

    private void addFolderMenuItemActionPerformed(ActionEvent evt) {//GEN-FIRST:event_addFolderMenuItemActionPerformed
        String path = JOptionPane.showInputDialog(this, "Path to a new directory:", new File("projects/" + Objects.requireNonNull(projectsTree.getSelectionPath()).getPath()[1]).getAbsolutePath());
        if (path != null) {
            new File(path).mkdirs();
        }
    }//GEN-LAST:event_addFolderMenuItemActionPerformed

    private void renameMenuItemActionPerformed(ActionEvent evt) {//GEN-FIRST:event_renameMenuItemActionPerformed
        new File("projects/" + Objects.requireNonNull(projectsTree.getSelectionPath()).getPath()[1]).renameTo(new File(JOptionPane.showInputDialog(this, "New project name:", String.valueOf(projectsTree.getSelectionPath().getPath()[1]))));
    }//GEN-LAST:event_renameMenuItemActionPerformed

    private void deleteMenuItemActionPerformed(ActionEvent evt) {//GEN-FIRST:event_deleteMenuItemActionPerformed
        this.deleteProjectActionPerformed(evt);
    }//GEN-LAST:event_deleteMenuItemActionPerformed

    private void propertiesMenuItemActionPerformed(ActionEvent evt) {//GEN-FIRST:event_propertiesMenuItemActionPerformed
        JOptionPane.showMessageDialog(this, "Project name: " + Objects.requireNonNull(projectsTree.getSelectionPath()).getPath()[1] + "\nProject path:" + new File("projects/" + projectsTree.getSelectionPath().getPath()[1]).getAbsolutePath() + "\nCreated with " + this.getTitle(), projectsTree.getSelectionPath().getPath()[1] + " project properties", JOptionPane.INFORMATION_MESSAGE);
    }//GEN-LAST:event_propertiesMenuItemActionPerformed

    private void saveButtonActionPerformed(ActionEvent evt) {//GEN-FIRST:event_saveButtonActionPerformed
        this.saveActionPerformed(evt);
    }//GEN-LAST:event_saveButtonActionPerformed

    private void deleteProjectButtonActionPerformed(ActionEvent evt) {//GEN-FIRST:event_deleteProjectButtonActionPerformed
        this.deleteProjectActionPerformed(evt);
    }//GEN-LAST:event_deleteProjectButtonActionPerformed

    private void runOCEmuButtonActionPerformed(ActionEvent evt) {//GEN-FIRST:event_runOCEmuButtonActionPerformed
        runOCEmu();
    }//GEN-LAST:event_runOCEmuButtonActionPerformed

    private void runOcelotButtonActionPerformed(ActionEvent evt) {//GEN-FIRST:event_runOcelotButtonActionPerformed
        runOcelot();
        //new OcelotEmulatorFrame().setVisible(true);
    }//GEN-LAST:event_runOcelotButtonActionPerformed

    private void openFileActionPerformed(ActionEvent evt) {//GEN-FIRST:event_openFileActionPerformed
        JFileChooser fileChooser = new OpenFileFileChooser();
        if (fileChooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
            try {
                openFile(fileChooser.getSelectedFile());
            } catch (FileNotFoundException ex) {
                Logger.getLogger(EditorFrame.class.getName()).log(Level.SEVERE, "File not found", ex);
            }
        }
    }//GEN-LAST:event_openFileActionPerformed

    private void createFileActionPerformed(ActionEvent evt) {//GEN-FIRST:event_createFileActionPerformed
        this.addFileMenuItemActionPerformed(evt);
    }//GEN-LAST:event_createFileActionPerformed

    private void ocemulatorMenuItemActionPerformed(ActionEvent evt) {//GEN-FIRST:event_ocemulatorMenuItemActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_ocemulatorMenuItemActionPerformed

    private void aurumMenuItemActionPerformed(ActionEvent evt) {//GEN-FIRST:event_aurumMenuItemActionPerformed
        Thread aurum = new Thread(() -> new AurumEmulatorForm().setVisible(true));
        aurum.start();
        throw new UnsupportedOperationException("Not supported yet");
    }//GEN-LAST:event_aurumMenuItemActionPerformed

    private void codeMenuItemActionPerformed(ActionEvent evt) {//GEN-FIRST:event_codeMenuItemActionPerformed
        Thread code = new Thread(() -> new CODEForm().setVisible(true));
        code.start();
        throw new UnsupportedOperationException("Not supported yet");
    }//GEN-LAST:event_codeMenuItemActionPerformed

    private void settingsMenuItemActionPerformed(ActionEvent evt) {//GEN-FIRST:event_settingsMenuItemActionPerformed
        new SettingsFrame().setVisible(true);
    }//GEN-LAST:event_settingsMenuItemActionPerformed

    /**
     * Attempts to open
     * {@link OCEmuLauncher}
     */
    private void runOCEmu() {
        if (projectsTree.getSelectionPath() != null) {
            if (projectsTree.getSelectionPath().getPath().length > 1 && projectsTree.getSelectionPath().getPath().length < 3) {
                if (String.valueOf(projectsTree.getSelectionPath().getPath()[1]) == null || "".equals(String.valueOf(projectsTree.getSelectionPath().getPath()[1]))) {
                    JOptionPane.showMessageDialog(this, "Invalid project selection.", "Error: Can't run OCEmu", JOptionPane.ERROR_MESSAGE);
                } else {
                    new OCEmuForm().setVisible(true);
                }
            }
        } else {
            JOptionPane.showMessageDialog(this, "No project chosen. Please select a project folder in the file tree and then launch OCEmu.", "Project not set", JOptionPane.ERROR_MESSAGE);
        }
    }

    /**
     * Attempts to create a new instance of {@link java.lang.Process} with
     * <b>Ocelot Desktop</b> running in it
     */
    private void runOcelot() {
        try {
            ProcessBuilder pb = new ProcessBuilder("java", "-jar", "Ocelot" + FileSystems.getDefault().getSeparator() + "ocelot.jar");
            pb.redirectErrorStream(true);
            Process p = pb.start();
            BufferedReader r = new BufferedReader(new InputStreamReader(p.getInputStream()));
            String outLine;
            Logger.getLogger(EditorFrame.class.getName()).log(Level.INFO, "Starting Ocelot Desktop...");
            while (true) {
                outLine = r.readLine();
                if (outLine == null) {
                    break;
                }
                System.out.println(outLine);
            }
        } catch (IOException ex) {
            Logger.getLogger(EditorFrame.class.getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(null, ex, "Caught " + ex.getClass().getName(), JOptionPane.ERROR_MESSAGE);
        }
    }

    private JTabbedPane editorTabs;
    private JPopupMenu fileManagementPopup;
    public static JTextArea outputTextArea;
    private JPopupMenu projectManagementPopup;
    private static JTree projectsTree;
    private JTree variableTree;
    // End of variables declaration//GEN-END:variables
}
